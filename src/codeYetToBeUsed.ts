/*

const goalsRef: AngularFirestoreCollection<any> = this.afs.collection(
  //   `users/${user.uid}/goals`
  // ).valueChanges()

-------------CHECKMARK SVG AND SASS---------
<div class="checkCont" (click) = "isChecked = !isChecked">
    <svg class="checkmark" [class.checked] = "isChecked" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"><circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/><path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>
 </div>
            
            
.checkCont
    position: relative
    width: 30px 
    border-radius: 1000px
    cursor: pointer

svg 
    background-size: contain          
            
---------------DROPZONE----------------          
 <div
         class = "dropzone" 
         dropZone
         (hovered) = "toggleHover($event)"
         (dropped) = "startUpload($event)"
         [class.hovering] = "isHovering"
         >
            <h3>Drop Zone</h3>
            <p>Drag and Drop a File</p>
            <div>
                <label class="file-label">
                or choose a file...
                <input type="file" (change) = "startUpload($event.target.files)">            
                </label>
            </div>
        </div>


        <div *ngIf = "percentage | async as pct">
            <progress [value] = "pct" max = "100"></progress>    
        </div>
       
        <div *ngIf = "snapshot | async as snap">
            {{ snap.bytesTransferred}} of {{ snap.totalBytes}} 

            <div *ngIf="downloadURL | async as url">
                <h3>Results!</h3>
                <img [src]="url"><br>
                <a [href]="url" target="_blank" rel="noopener">Download Me!</a>
            </div> 

        </div>            
            
            
   //main task
  task: AngularFireUploadTask;
  //progress monitoring
  percentage: Observable<number>;

  snapshot: Observable<any>;

  //Download URL
  downloadURL: Observable<string>;

  //state for dropzone css toggle
  isHovering: boolean;

  constructor(
    private storage: AngularFireStorage,
    private authService: AuthService
  ) {}

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  startUpload(event: FileList) {
    //file object
    const file = event.item(0);
    //client-side validation
    if (file.type.split("/")[0] != "image") {
      //do error
      console.error("unsupported file type");
      return;
    }
    //file storage path
    const path = `${this.authService.user.uid}/${new Date().getTime()}_${
      file.name
    }`;
    //optional metadata
    const customMetadata = { app: "Goalie" };

    this.task = this.storage.upload(path, file, { customMetadata });

    //progress monitoring
    this.percentage = this.task.percentageChanges();
    this.snapshot = this.task.snapshotChanges().pipe(
      tap(snap => {
        if(snap.bytesTransferred === snap.totalBytes){
          this.db.collection().set()
        }
      }))

    //file download URL
    this.downloadURL = this.task.downloadURL();
  }

  //Determine if upload task is active
  isActive(snapshot) {
    return (
      snapshot.state === "running" &&
      snapshot.bytesTransferred < snapshot.totalBytes
    );
  }           
            
            
            
            
            
*/
