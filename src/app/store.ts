import { Goal } from "./goals/my-goals/goals.service";
import { Message } from "./messages/messages.service";
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

import { User } from "./auth/shared/auth.service";

import "rxjs/add/operator/pluck";
import "rxjs/add/operator/distinctUntilChanged";

export interface State {
  user: User;
  usersList: User[];
  messages: Message[];
  goals: Goal[];
  [key: string]: any;
}

const state: State = {
  user: undefined,
  usersList: undefined,
  messages: undefined,
  goals: undefined
};

export class Store {
  private subject = new BehaviorSubject<State>(state);
  private store = this.subject.asObservable().distinctUntilChanged();

  get value() {
    return this.subject.value;
  }

  select<T>(name: string): Observable<T> {
    return this.store.pluck(name);
  }

  set(name: string, state: any) {
    this.subject.next({ ...this.value, [name]: state });
  }
}
