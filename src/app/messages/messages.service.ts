//import { Store } from "./../store";
import { Observable } from "rxjs/Observable";
import { AngularFireStorage } from "angularfire2/storage";
import { Injectable } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection
} from "angularfire2/firestore";
import { take, tap } from "rxjs/operators";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

export interface QueryConfig {
  path: string;
  field: string;
  limit?: number;
  reverse?: boolean;
  prepend?: boolean;
}

export interface Message {
  image: string;
  text: string;
  uid: string;
  created: number;
  id: string;
}

export interface UnpreparedMessage {
  image: FileList;
  text: string;
  uid: string;
  created: number;
}

@Injectable()
export class MessagesService {
  // messages$ = this.fireStore
  //   .collection("messages", ref => {
  //     return ref.orderBy("created");
  //   })
  //   .valueChanges()
  //   .pipe(
  //     tap(messages => {
  //       this.store.set("messages", messages);
  //     })
  //   );

  private _done = new BehaviorSubject(false);
  private _loading = new BehaviorSubject(false);
  private _data = new BehaviorSubject([]);

  private query: QueryConfig;

  // Observable data
  data: Observable<any>;
  done: Observable<boolean> = this._done.asObservable();
  loading: Observable<boolean> = this._loading.asObservable();
  gettingMore = false;
  constructor(
    private fireStorage: AngularFireStorage,
    private fireStore: AngularFirestore // private store: Store
  ) {}

  // Initial query sets options and defines the Observable
  // passing opts will override the defaults
  init(path: string, field: string, opts?: any) {
    this.query = {
      path,
      field,
      limit: 10,
      reverse: false,
      prepend: false,
      ...opts
    };

    const first = this.fireStore.collection(this.query.path, ref => {
      return ref
        .orderBy(this.query.field, this.query.reverse ? "desc" : "asc")
        .limit(this.query.limit);
    });

    this.mapAndUpdate(first);

    // Create the observable array for consumption in components
    this.data = this._data.asObservable().scan((acc, val) => {
      let items = [];
      val.forEach(v => {
        for (let index = 0; index < acc.length; index++) {
          const a = acc[index];
          if (a.doc.id === v.doc.id) {
            acc[index] = v; // replace the current doc with the updated doc.
            return;
          }
        }
        items.push(v); // if the doc is not found from the current list, append it
      });

      if (this.gettingMore) {
        this.gettingMore = false;
        return this.query.prepend ? items.concat(acc) : acc.concat(items);
      } else {
        return this.query.prepend ? acc.concat(items) : items.concat(acc);
      }
    });
  }

  // Retrieves additional data from firestore
  more() {
    this.gettingMore = true;
    const cursor = this.getCursor();

    const more = this.fireStore.collection(this.query.path, ref => {
      return ref
        .orderBy(this.query.field, this.query.reverse ? "desc" : "asc")
        .limit(this.query.limit)
        .startAfter(cursor);
    });
    this.mapAndUpdate(more);
  }

  // Determines the doc snapshot to paginate query
  private getCursor() {
    const current = this._data.value;
    if (current.length) {
      return this.query.prepend
        ? current[0].doc
        : current[current.length - 1].doc;
    }
    return null;
  }

  // Maps the snapshot to usable format the updates source
  private mapAndUpdate(col: AngularFirestoreCollection<any>) {
    if (this._done.value || this._loading.value) {
      return;
    }

    // loading
    this._loading.next(true);

    // Map snapshot with doc ref (needed for cursor)
    return col
      .snapshotChanges()
      .pipe(
        tap(arr => {
          let values = arr.map(snap => {
            const data = snap.payload.doc.data();
            const doc = snap.payload.doc;
            return { ...data, doc };
          });

          // If prepending, reverse the batch order
          values = this.query.prepend ? values.reverse() : values;

          // update source with new values, done loading
          this._data.next(values);
          this._loading.next(false);

          // no more values, mark done
          if (!values.length) {
            this._done.next(true);
          }
        })
      )
      .subscribe();
  }

  //-------------------------------------------------------

  newMessage(message) {
    return this.uploadPicture(message.image)
      .pipe(
        take(1),
        tap(url => {
          message.image = url;
          this.fireStore.collection("messages").add({
            image: message.image,
            created: message.created,
            text: message.text,
            uid: message.uid,
            id: this.fireStore.createId()
          });
        })
      )
      .subscribe();
  }

  uploadPicture(event: FileList): Observable<string> {
    //no file
    if (event === null) return Observable.of(null);

    //file object
    const file = event.item(0);

    //client-side validation
    if (file.type.split("/")[0] != "image") {
      console.error("unsupported file type");
      return Observable.of(null);
      // return Promise.reject(new Error("unsupported file type"));
    }

    //file storage path
    const path = `messageImages/${Date.now()}_${file.name}`;

    //optional metadata
    const customMetadata = { app: "Goalie" };

    let task = this.fireStorage.upload(path, file, { customMetadata });

    return task.downloadURL();
  }
}
