import { User } from "./../../auth/shared/auth.service";
import { Message } from "./../messages.service";
import {
  Component,
  Input,
  ViewChild,
  ViewChildren,
  ElementRef,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  QueryList
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "messages-list",
  styleUrls: ["messages-list.component.sass"],
  template: `
        <div class = "messages-list"
        #messageBox
        scrollable
        (scrollPosition)="scrollHandler($event)"
        >
            <div #forBox *ngFor = "let message of messages; trackBy: trackByFn;" class = "message-section" >
            
                <div *ngIf="message.text; else imgMessage" class="message text"  [class.own]="message.uid === uid">
                    {{message.text}}
                    <div class="profile-pic" [ngStyle]="{'background-image': 'url(' + findProfilePic(message.uid) + ')'}">
                    <span class="userName">{{findUserName(message.uid)}}</span></div> 
                </div>   
                <ng-template #imgMessage>
                    <div class="message image" [class.own]="message.uid === uid">
                        <img [src]="message.image" class = "main-image">
                       <div class="profile-pic" [ngStyle]="{'background-image': 'url(' + findProfilePic(message.uid) + ')'}">
                          <span class="userName">{{findUserName(message.uid)}}</span>
                       </div>  
                    </div>                                        
                </ng-template>   
            </div>
            </div>
            <div class="readPopup"  (click)="scrollDownIcon()"></div>
    `
})
export class MessagesListComponent {
  messages: Message[];
  oldScrollheight: number;
  oldScrollTop: number;
  readMessage: boolean;
  firstload = true;

  @ViewChild("messageBox") messageBox: ElementRef;
  @ViewChildren("forBox") forBox: QueryList<any>;
  @Output() loadMore = new EventEmitter();
  @Input()
  set _messages(value) {
    this.oldScrollheight = this.messageBox.nativeElement.scrollHeight;
    this.oldScrollTop = this.messageBox.nativeElement.scrollTop;
    this.messages = value;
  }

  @Input() usersList: User[];
  @Input() uid: string;

  scrollDownIcon() {
    this.scrollBottom();
  }
  ngAfterViewInit() {
    this.forBox.changes.subscribe(() => {
      this.ngForCallback();
    });
  }

  ngForCallback() {
    if (this.firstload) {
      this.firstload = false;
      this.scrollBottom();
    } else this.keepScroll();
  }

  loadMoreMessages() {
    this.loadMore.emit();
  }

  scrollHandler(event) {
    if (event === "top") {
      this.loadMoreMessages();
    }
    if (event === "bottom") {
      this.readMessage = true;
    }
  }
  scrollBottom() {
    this.readMessage = true;
    let elem = this.messageBox.nativeElement.querySelector(
      ".message-section:last-child"
    );

    if (elem) {
      elem.scrollIntoView({
        behavior: "smooth"
      });
    }
  }

  keepScroll() {
    this.messageBox.nativeElement.scrollTop =
      this.oldScrollTop +
      this.messageBox.nativeElement.scrollHeight -
      this.oldScrollheight;
  }

  trackByFn(index, item) {
    return item.id; // or item.id
  }

  findProfilePic(uid) {
    let user = this.usersList.find(elem => {
      return elem.uid === uid;
    });
    if (user) {
      return user.profilePicURL;
    } else {
      return "";
    }
  }
  findUserName(uid) {
    return this.usersList.find(elem => {
      return elem.uid === uid;
    }).username;
  }
}
