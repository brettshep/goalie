import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";

//module
import { InputBarModule } from "./../shared/inputbar.module";
import { SpinnerModule } from "../shared/spinner/spinner.module";

//components
import { MessagesComponent } from "./containers/messages.component";
import { MessagesListComponent } from "./componenets/messages-list.component";
//service
import { MessagesService } from "./messages.service";
//directive
import { ScrollableModule } from "../shared/scrollable.module";

export const ROUTES: Routes = [
  {
    path: "",
    //canActivate: [AuthGuard],
    component: MessagesComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    InputBarModule,
    ScrollableModule,
    SpinnerModule
  ],
  declarations: [MessagesComponent, MessagesListComponent],
  providers: [MessagesService]
})
export class MessagesModule {}
