import { Store } from "./../../store";
import { AuthService, User } from "./../../auth/shared/auth.service";
import {
  Message,
  MessagesService,
  UnpreparedMessage
} from "./../messages.service";
import { Observable } from "rxjs/Observable";
import { Component } from "@angular/core";

@Component({
  selector: "messages",
  template: `
    <div *ngIf="(messages$ | async) as messages; else loadingMessages">
      <div *ngIf="(usersList$ | async) as usersList; else loadingUsers">
          <messages-list
          [_messages]="messages"
          [uid]="authService.user?.uid"
          [usersList]="usersList"
          (loadMore)="loadMoreMessages()"
          >
          </messages-list>
          <input-bar
          [placeHolder]="'Type a message...'"
          [isLoading] = "isLoading"
          [showImageAdd]="true"
          (picSend)="handlePic($event)" 
          (textSend)="handleText($event)"
          ></input-bar> 
      </div>
    </div>
        <ng-template #loadingMessages>
          <spinner [color]="'#ccc'" [size]="'50px'"></spinner>
        </ng-template>

        <ng-template #loadingUsers>
        Loading Users...
        </ng-template>
    `
})
export class MessagesComponent {
  constructor(
    private messageService: MessagesService,
    private authService: AuthService,
    private store: Store
  ) {}

  // subscriptions: Subscription[];
  messages$: Observable<Message[]>;
  usersList$: Observable<User[]>;
  isLoading: boolean = false;

  ngOnInit() {
    //this.subscription = this.messageService.messages$.subscribe();
    //this.messages$ = this.store.select<Message[]>("messages");
    this.usersList$ = this.store.select<User[]>("usersList");
    this.messageService.init("messages", "created", {
      reverse: true,
      prepend: true
    });

    this.messages$ = this.messageService.data;
  }

  ngOnDestroy() {
    // this.subscriptions.forEach(sub => {
    //   sub.unsubscribe();
    // });
  }

  loadMoreMessages() {
    this.messageService.more();
  }

  handlePic(file: FileList) {
    this.isLoading = true;
    const message: UnpreparedMessage = {
      uid: this.authService.UID,
      created: Date.now(),
      text: "",
      image: file
    };

    this.messageService.newMessage(message).add(() => {
      this.isLoading = false;
    });
  }

  handleText(text: string) {
    this.isLoading = true;
    const message: UnpreparedMessage = {
      uid: this.authService.UID,
      created: Date.now(),
      text,
      image: null
    };
    this.messageService.newMessage(message).add(() => {
      this.isLoading = false;
    });
  }
}
