import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "time"
})
export class TimePipe implements PipeTransform {
  transform(value: number): string {
    const hours: number = (value / 3600) >> 0;
    const minutes: number = ((value - hours * 3600) / 60) >> 0;
    const seconds: number = (value - hours * 3600 - minutes * 60) >> 0;
    if (hours > 0) {
      return (
        hours.toString().padStart(2, "0") +
        ":" +
        minutes.toString().padStart(2, "0") +
        ":" +
        seconds.toString().padStart(2, "0")
      );
    } else {
      return (
        minutes.toString().padStart(2, "0") +
        ":" +
        seconds.toString().padStart(2, "0")
      );
    }

    //return minutes + ":" + (value - minutes * 60);
  }
}
