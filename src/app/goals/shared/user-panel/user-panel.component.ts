import { User } from "@firebase/auth-types";
import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "user-panel",
  styleUrls: ["user-panel.component.sass"],
  template: `
    <div class="wrapper">

        <div class="user-panel">
            <div 
              *ngFor="let user of users"
              class="image-cont"
              (click)="onClick(user)"
              [ngStyle]="{'background-image': 'url(' + user.profilePicURL + ')'}"
              [class.active] = "user.uid === currId"
              >
              <span class="userName">{{user.username}}</span>
            </div>
        </div>

  </div>
    `
})
export class UserPanelComponent {
  @Input() users;
  @Input() currId;
  @Output() uid = new EventEmitter<string>();

  onClick(user: User) {
    this.uid.emit(user.uid);
  }
}
