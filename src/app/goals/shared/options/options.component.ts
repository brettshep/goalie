import {
  Component,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  Input
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "options-panel",
  styleUrls: ["options.component.sass"],
  template: `
        <div 
        class = "options-panel" 
        clickedOutside
        (clickedOutside) = "toggleClose()"

        dropZone
        (hovered)="toggleHover($event)"
        (dropped)="uploadPic($event)"
        [class.hovering] = "isHovering"
        >
            <img [src]="timerIconSrc" (click)="toggleTimer()" *ngIf="!goal.complete">
            <div class="image-box" *ngIf="!goal.complete">
                <input type="file" (change)="uploadPic($event.target.files)">
                <img src="/img/picture.svg">
            </div>
            <img src="/img/trash.svg"  (click)="deleteGoal()">
        </div>
    `
})
export class OptionsComponent {
  @Output() delete = new EventEmitter();
  @Output() close = new EventEmitter();
  @Output() picChoosen = new EventEmitter();
  @Output() startTimer = new EventEmitter();
  @Output() stopTimer = new EventEmitter();
  @Input() timerOn: boolean;
  @Input() goal;
  clickedYet: boolean = true;
  isHovering: boolean = false;

  toggleTimer() {
    if (!this.timerOn) this.startTimerEmit();
    else this.stopTimerEmit();
    this.close.emit();
  }
  startTimerEmit() {
    this.timerOn = true;
    this.startTimer.emit();
  }

  stopTimerEmit() {
    this.timerOn = false;
    this.stopTimer.emit();
  }

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  toggleClose() {
    this.clickedYet = !this.clickedYet;
    if (this.clickedYet) this.close.emit();
  }

  deleteGoal() {
    this.delete.emit();
    this.close.emit();
  }

  uploadPic(filelist) {
    this.picChoosen.emit(filelist);
    this.close.emit();
  }

  get timerIconSrc() {
    if (this.timerOn) {
      return "/img/pause.svg";
    } else {
      return "/img/play.svg";
    }
  }
}
