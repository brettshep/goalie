import { Goal } from "../../my-goals/goals.service";
import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef
} from "@angular/core";
import { TimerObservable } from "rxjs/observable/TimerObservable";
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: "goal",
  styleUrls: ["goal.component.sass"],
  template: `
  <div class = "cont" [class.delete]="deleteAnim">
        <div class = "goal" [class.active]="active">
   
            <div class="checkCont"  (click) = "toggleGoalComplete(!goal.complete)">
                <svg class="checkmark" [class.checked] = "isChecked" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"><circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/><path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>
            </div>
            <div class="text">
                {{goal.text}}
            </div>
            
            <div *ngIf="goal.image" class="goal-image-icon" (mouseenter)="mouseEnter()" (mouseleave)="mouseLeave()">
                <img [src]="imageIconSrc">        
            </div>

            <div class="goal-image" *ngIf="imageHover">
                <img [src]="goal.image" (load)="loaded()">
            </div>
            
            <div class="goal-time" *ngIf="currTime !== 0">
                <span [class.active]="timerOn" (click)="toggleTimer()">{{currTime | time}}</span>
            </div>

            <div class="options" >
                <div class="ellipsis" (click)="optionsToggleFunct()">
                    <img src="/img/ellipsis.svg" >
                </div>
                <options-panel
                *ngIf="optionsToggle"
                (delete)="deleteGoalAnim()"
                (close)="optionsToggle = !optionsToggle"
                (picChoosen)="uploadPic($event, goal.id)"
                (startTimer)="startTimer()"
                (stopTimer)="stopTimer()"
                [timerOn]="timerOn"
                [goal]="goal"
                >
                </options-panel>
            </div>
        </div>
      </div>
    `
})
export class GoalComponent {
  goal: Goal;
  active: boolean = false;
  deleteAnim: boolean = false;
  optionsToggle: boolean = false;
  @Input() uid;
  @Input()
  set _goal(value) {
    this.goal = value;
    if (value.image !== "") {
      this.setimageIconSrc(false);
    }
    if (!this.timerOn) this.currTime = value.loggedTime;

    this.isChecked = value.complete;
  }

  @Output() delete = new EventEmitter<string>();
  @Output() picChoosen = new EventEmitter();
  @Output() updateDBTime = new EventEmitter<number>();
  @Output() timerRunning = new EventEmitter<boolean>();
  @Output() toggleComplete = new EventEmitter<boolean>();

  ngOnInit() {
    setTimeout(() => {
      this.active = true;
    }, 100);
  }
  //   @Output() updateStoreTime = new EventEmitter();
  optionsToggleFunct() {
    if (!this.authCheck()) {
      return;
    }
    this.optionsToggle = !this.optionsToggle;
  }

  //image icon
  imageHover: boolean = false;
  imageLoaded: boolean = false;
  imageIconSrc: string = "/img/picture.svg";
  //timer
  currTime: number;
  timer: Subscription;
  timerOn: boolean = false;
  //checkmark
  isChecked: boolean = false;
  constructor(private change: ChangeDetectorRef) {}

  authCheck(): boolean {
    if (this.uid !== this.goal.uid) console.log("not authorized!");
    return this.uid === this.goal.uid;
  }
  toggleGoalComplete(completeState) {
    if (!this.authCheck()) {
      return;
    }

    if (this.timerOn) {
      this.stopTimer();
    }
    this.isChecked = completeState;
    setTimeout(() => {
      this.deleteAnim = true;
    }, 300);

    setTimeout(() => {
      this.toggleComplete.emit(completeState);
    }, 600);
  }

  toggleTimer() {
    if (!this.authCheck()) {
      return;
    }
    if (!this.goal.complete) {
      if (this.timerOn) this.stopTimer();
      else this.startTimer();
    }
  }

  startTimer() {
    if (!this.authCheck()) {
      return;
    }
    this.timerOn = true;
    this.timerRunning.emit(true);
    this.timer = TimerObservable.create(0, 1000).subscribe(() => {
      this.currTime++;
      //   this.updateStoreTime.emit({ time: this.currTime, running: true });
      this.change.markForCheck();
    });
  }

  stopTimer() {
    if (!this.authCheck()) {
      return;
    }
    this.timerOn = false;
    this.timer.unsubscribe();
    // this.updateStoreTime.emit({ time: this.currTime, running: false });
    this.updateDBTime.emit(this.currTime);
    this.timerRunning.emit(false);
  }

  mouseEnter() {
    this.imageHover = true;
    if (!this.imageLoaded) this.setimageIconSrc(true);
  }

  mouseLeave() {
    this.imageHover = false;
    this.setimageIconSrc(false);
  }

  loaded() {
    this.imageLoaded = true;
    this.setimageIconSrc(false);
  }

  deleteGoalAnim() {
    this.deleteAnim = true;
    setTimeout(() => {
      this.deleteGoal();
    }, 300);
  }
  deleteGoal() {
    if (!this.authCheck()) {
      return;
    }
    this.delete.emit(this.goal.id);
  }

  uploadPic(filelist, id) {
    if (!this.authCheck()) {
      return;
    }
    this.picChoosen.emit({ filelist, id });
    this.setimageIconSrc(true);
  }

  setimageIconSrc(loading: boolean) {
    if (loading) this.imageIconSrc = "/img/loadingDarkSmall.svg";
    else this.imageIconSrc = "/img/picture.svg";
  }
}
