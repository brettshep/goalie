import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";

//directice
import { ClickedOutsideModule } from "../../shared/clickedOutside.module";
import { DropZoneModule } from "../../shared/dropZone.module";

//pipes
import { TimePipe } from "../../pipes/time.pipe";

//components
import { UserPanelComponent } from "./user-panel/user-panel.component";
import { GoalComponent } from "./goal/goal.component";
import { OptionsComponent } from "./options/options.component";

@NgModule({
  imports: [CommonModule, ClickedOutsideModule, DropZoneModule],
  declarations: [UserPanelComponent, GoalComponent, OptionsComponent, TimePipe],
  exports: [UserPanelComponent, GoalComponent, OptionsComponent, TimePipe]
})
export class GoalsSharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: GoalsSharedModule,
      providers: []
    };
  }
}
