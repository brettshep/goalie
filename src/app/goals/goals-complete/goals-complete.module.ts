import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
//modules
import { GoalsSharedModule } from "./../shared/goals-shared.module";
import { InputBarModule } from "./../../shared/inputbar.module";

//services
import { GoalsCompleteService } from "./goals-complete.service";

//components
import { CompleteGoalListComponent } from "./components/complete-goal-list.component";
import { DateControlsComponent } from "./components/date-controls/date-controls.component";

//containers
import { GoalsCompleteComponent } from "./containers/goals-complete.component";

export const ROUTES: Routes = [
  {
    path: "",
    component: GoalsCompleteComponent
  },
  { path: ":id", component: GoalsCompleteComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    GoalsSharedModule,
    InputBarModule
  ],
  declarations: [
    GoalsCompleteComponent,
    CompleteGoalListComponent,
    DateControlsComponent
  ],
  providers: [GoalsCompleteService]
})
export class GoalsCompleteModule {}
