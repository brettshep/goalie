import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "date-controls",
  styleUrls: ["date-controls.component.sass"],
  template: `
        <div class="date-controls" [class.hasBG]="user?.bg">
 
                <button (click)="dateLeft(true)"></button>
                {{start | date: 'MMMM d'}} - {{end | date: 'MMMM d'}}
                <button (click)="dateLeft(false)"></button>

        </div>
    `
})
export class DateControlsComponent {
  start: Date;
  end: Date;
  @Input() user;
  @Input()
  set dateStart(value) {
    this.start = value;
    this.end = new Date(
      value.getFullYear(),
      value.getMonth(),
      value.getDate() + 6
    );
  }
  @Output() moveLeft = new EventEmitter<boolean>();

  dateLeft(event) {
    this.moveLeft.emit(event);
  }
}
