import {
  Component,
  Input,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter,
  ChangeDetectionStrategy
} from "@angular/core";
import { Goal } from "../../my-goals/goals.service";

@Component({
  selector: "complete-goal-list",
  styleUrls: ["complete-goal-list.component.sass"],
  template: `
        <div class="wrapper">
            <div class = "complete-goal-list" #goalBox>
                <div class="spacer">
                    <date-controls [user]="user" [dateStart]="_dateStart" (moveLeft)="dateChange($event)"></date-controls>
                    <div>
                    <div *ngFor="let goal of goals; trackBy: trackByFn; index as i">
                   <div class="dayHeader" *ngIf="indices[i]" [class.hasBG]="user?.bg"> {{indices[i] | date: 'EEEE, MMMM d'}}</div>
                          <goal 
                          [_goal]="goal"
                          [uid]="uid"
                          (delete)="deleteGoal($event)"
                          (toggleComplete)="toggleCompleteEmit($event,goal.id)"
                          ></goal>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `
})
export class CompleteGoalListComponent {
  //------VARS-------
  headerDate: Date = new Date();
  goals: Goal[] = [];
  newGoalScroll: boolean;
  changed: boolean = true;
  _dateStart: Date;
  indices: {};

  //---INPUT-OUTPUT-VIEWCHILD--
  @ViewChild("goalBox") goalBox: ElementRef;
  @Input() user;
  @Input() //currId input
  set dateStart(value) {
    if (this._dateStart !== value) {
      this.changed = true;
    }
    this._dateStart = value;
  }
  @Input() uid;
  @Input() //goals input
  set _goals(value: Goal[]) {
    value = value.reverse();
    this.createIndices(value);

    if (!this.changed) {
      this.goals = value;
    } else {
      this.goals = [];
      for (let i = 0; i < value.length; i++) {
        let goal = value[i];
        setTimeout(() => {
          this.goals.push(goal);
        }, 100 * i);
      }
    }
    this.changed = false;
  }

  @Output() delete = new EventEmitter<string>();
  @Output() toggleComplete = new EventEmitter();
  @Output() changeDate = new EventEmitter<Boolean>();

  // nameOfDayOfWeek(dayOfWeek) {
  //   return this.daysOfTheWeek[dayOfWeek];

  //   // new Date(
  //   //   day.getFullYear(),
  //   //   day.getMonth(),
  //   //   day.getDate() + 7
  //   // ).getTime() - 1;
  // }

  createIndices(goals) {
    this.indices = {};
    let currDate = this._dateStart;
    let currDay = currDate.getDay();

    for (let i = 0; i < goals.length; i++) {
      const iDay = new Date(goals[i].created);
      if (currDay !== iDay.getDay()) {
        this.indices[i] = iDay;
        currDay = iDay.getDay();
      }
    }
  }

  dateChange(event) {
    this.changeDate.emit(event);
  }

  toggleCompleteEmit(complete: boolean, id: string) {
    this.toggleComplete.emit({ complete, id });
  }

  deleteGoal(id: string) {
    this.delete.emit(id);
  }

  trackByFn(index, item) {
    return item.id; // or item.id
  }
}
