import { Injectable } from "@angular/core";
import { AuthService } from "../../auth/shared/auth.service";
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Store } from "../../store";
import { AngularFirestore } from "angularfire2/firestore";
import { switchMap, tap, take } from "rxjs/operators";
import { Goal } from "../my-goals/goals.service";

@Injectable()
export class GoalsCompleteService {
  currentId$: BehaviorSubject<string>;
  startOfWeek$: BehaviorSubject<Date>;
  goals$: Observable<Goal[]>;
  constructor(
    private fireStore: AngularFirestore,
    private store: Store,
    private authService: AuthService
  ) {
    this.currentId$ = new BehaviorSubject(this.authService.UID);
    this.startOfWeek$ = new BehaviorSubject(this.getStartOfWeek(new Date()));
    //change Goals
    this.goals$ = this.startOfWeek$.pipe(
      switchMap(day => {
        // const startAt = (
        //     new Date(day.getFullYear(), day.getMonth(), day.getDate())
        //   ).getTime();
        const startAt = day.getTime() - 1;
        const endAt =
          new Date(
            day.getFullYear(),
            day.getMonth(),
            day.getDate() + 7
          ).getTime() - 1;

        return this.fireStore
          .collection<Goal>("completeGoals", ref =>
            ref
              .where("created", ">", startAt)
              .where("created", "<", endAt)
              .where("uid", "==", this.currentId$.getValue())
              .orderBy("created")
          )
          .valueChanges();
      })
    );
  }

  changeDate(day: Date) {
    this.startOfWeek$.next(day);
  }

  getStartOfWeek(date: Date) {
    const day = date.getDay();
    const diff = date.getDate() - day;
    date.setDate(diff);
    date.setHours(0, 0, 0, 0);
    return date;
  }

  changeUid(id) {
    this.currentId$.next(id);
    const date = new Date(this.startOfWeek$.getValue());
    this.startOfWeek$.next(date);
  }

  toggleGoalComplete(complete, goalId) {
    const currdoc = this.fireStore.collection("completeGoals").doc(goalId);
    const myGoalsRef = this.fireStore.collection("myGoals");
    currdoc.ref.get().then(doc => {
      //set goal data
      let goal = doc.data();
      goal.created = Date.now();
      goal.complete = false;
      //push to other collection
      myGoalsRef.doc(goal.id).set(goal);
      currdoc.delete();
    });
    // .set({ complete, created: Date.now() }, { merge: true });
  }

  deleteGoal(goalId: string) {
    return this.fireStore
      .collection<Goal>("completeGoals")
      .doc(goalId)
      .delete();
  }
}
