import { Component } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Subscription } from "rxjs/Subscription";
import { Store } from "./../../../store";
import { User } from "@firebase/auth-types";
import { Router, ActivatedRoute } from "@angular/router";
import { Goal } from "../../my-goals/goals.service";
import { GoalsCompleteService } from "../goals-complete.service";
import { AuthService } from "../../../auth/shared/auth.service";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

@Component({
  selector: "goals-complete",
  styleUrls: ["goals-complete.component.sass"],
  template: `
        <div>
          <div *ngIf="(goals$ | async) as goals; else loading">
            <complete-goal-list
            [dateStart]="startOfWeek$.getValue()"
            [_goals]="goals"
            [uid]="uid"
            [user]="user$ | async"
            (delete)="deleteGoal($event)"       
            (toggleComplete)="toggleGoalComplete($event)"
            (changeDate)="changeDate($event)"
            ></complete-goal-list>
          </div>
          
          <div *ngIf="(usersList$ | async) as users;">        
              <user-panel
              [users]="users"
              [currId]= "currentId"
              (uid)="handleUserClicked($event)"
              ></user-panel>
          </div>
  <ng-template #loading>
    <div class="loading"></div>
  </ng-template>
        </div>
    `
})
export class GoalsCompleteComponent {
  constructor(
    private store: Store,
    private goalsService: GoalsCompleteService,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService
  ) {}
  usersList$: Observable<User[]>;
  goals$: Observable<Goal[]>;
  currentId: string;
  uid: string;
  user$;
  startOfWeek$;

  // weekStart$: BehaviorSubject<Date>;
  ngOnInit() {
    this.uid = this.authService.UID;
    this.user$ = this.authService.user$;
    this.startOfWeek$ = this.goalsService.startOfWeek$;
    this.usersList$ = this.store.select<User[]>("usersList"); //get user list
    this.goals$ = this.goalsService.goals$;
    // this.weekStart$ = this.goalsService.startOfWeek$;
    this.route.params.subscribe(param => {
      if (!param.id) {
        //navigate to root user if no id
        this.router.navigate([`/complete/${this.authService.UID}`]);
        return Observable.of(null);
      }

      this.goalsService.changeUid(param.id);
      this.currentId = param.id; //set current ID
    });
  }

  changeDate(moveLeft) {
    let sign = -1;
    if (!moveLeft) sign = 1;
    const oldDate = this.goalsService.startOfWeek$.getValue();
    const newDate = new Date(
      oldDate.getFullYear(),
      oldDate.getMonth(),
      oldDate.getDate() + 7 * sign
    );
    this.goalsService.startOfWeek$.next(newDate);
  }

  toggleGoalComplete(event) {
    this.goalsService.toggleGoalComplete(event.complete, event.id);
  }

  deleteGoal(goalId) {
    this.goalsService.deleteGoal(goalId);
  }

  handleUserClicked(uid: string) {
    //navigate to new user ID
    this.router.navigate([`/complete/${uid}`]);
  }
}
