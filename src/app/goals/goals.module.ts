import { GoalsSharedModule } from "./shared/goals-shared.module";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./../auth/authGuard.guard";

export const ROUTES: Routes = [
  {
    path: "mygoals",
    canActivate: [AuthGuard],
    loadChildren: "./my-goals/my-goals.module#MyGoalsModule"
  },
  {
    path: "complete",
    canActivate: [AuthGuard],
    loadChildren: "./goals-complete/goals-complete.module#GoalsCompleteModule"
  }
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES), GoalsSharedModule.forRoot()],
  declarations: [],
  providers: []
})
export class GoalsModule {}
