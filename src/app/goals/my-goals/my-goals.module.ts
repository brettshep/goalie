import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
//modules
import { GoalsSharedModule } from "./../shared/goals-shared.module";
import { InputBarModule } from "./../../shared/inputbar.module";

//services
import { GoalsService } from "./goals.service";

//components
import { MyGoalsListComponent } from "./components/my-goals-list/my-goals-list.component";

//containers
import { MyGoalsComponent } from "./containers/my-goals.component";

export const ROUTES: Routes = [
  {
    path: "",
    component: MyGoalsComponent
  },
  { path: ":id", component: MyGoalsComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    GoalsSharedModule,
    InputBarModule
  ],
  declarations: [MyGoalsComponent, MyGoalsListComponent],
  providers: [GoalsService]
})
export class MyGoalsModule {}
