import { Component } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Subscription } from "rxjs/Subscription";
import { GoalsService, Goal } from "../goals.service";
import { Store } from "./../../../store";
import { User } from "@firebase/auth-types";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthService } from "../../../auth/shared/auth.service";

@Component({
  selector: "my-goals",
  styleUrls: ["my-goals.component.sass"],
  template: `
        <div>
        <div class="timer-warning" [class.show]="timersRunning > 0">Recording time: If you leave, time won't be saved.</div>
          <div *ngIf="(goals$ | async) as goals; else loading">
            <my-goals-list
            [_newGoalScroll]="newGoalScroll"
            [currId]= "currentId"
            [_goals]="goals"
            [uid]="uid"
            (didScroll)="setScroll()"
            (delete)="deleteGoal($event)"   
            (picChoosen)="uploadPic($event)"      
            (updateDBTime)="updateDBTime($event)" 
            (timerRunning)="handleTimerRunning($event)"
            (toggleComplete)="toggleGoalComplete($event)"
            ></my-goals-list>
          </div>
          
          <div *ngIf="(usersList$ | async) as users;">        
              <user-panel
              [users]="users"
              [currId]= "currentId"
              (uid)="handleUserClicked($event)"
              ></user-panel>
          </div>

            <input-bar *ngIf="uid === currentId"
                [placeHolder]="'Create A Goal...'"
                [isLoading] = "false"
                [showImageAdd]="false"
                (textSend)="newGoal($event)"
            ></input-bar> 

            <ng-template #loading>
                <div class="loading"></div>
            </ng-template>
        </div>
    `
})
export class MyGoalsComponent {
  constructor(
    private store: Store,
    private goalsService: GoalsService,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService
  ) {}

  usersList$: Observable<User[]>;
  goals$: Observable<Goal[]>;
  uid: string;
  currentId: string;
  newGoalScroll = false;
  timersRunning = 0;

  ngOnInit() {
    this.uid = this.authService.UID;
    this.usersList$ = this.store.select<User[]>("usersList"); //get user list
    this.goals$ = this.goalsService.goals$;
    this.route.params.subscribe(param => {
      if (!param.id) {
        //navigate to root user if no id
        this.router.navigate([`/mygoals/${this.authService.UID}`]);
        return Observable.of(null);
      }
      this.goalsService.currentId$.next(param.id);
      this.currentId = param.id; //set current ID
    });
  }

  toggleGoalComplete(event) {
    this.goalsService.toggleGoalComplete(event.complete, event.id); //toggle complete
  }

  handleTimerRunning(event) {
    //deal with goal timers
    if (event === true) {
      this.timersRunning++;
    } else {
      this.timersRunning--;
    }
  }

  newGoal(text) {
    //make new goal
    this.newGoalScroll = true;
    const goal: Goal = {
      id: this.goalsService.newId,
      uid: this.currentId,
      created: Date.now(),
      text,
      loggedTime: 0,
      image: "",
      complete: false
    };
    this.goalsService.createNewGoal(goal);
  }

  deleteGoal(goalId) {
    //delete goal
    this.goalsService.deleteGoal(goalId);
  }

  uploadPic(event) {
    this.goalsService.setGoalImage(event.filelist, this.currentId, event.id); //upload goal pic
  }

  updateDBTime(timeObj) {
    this.goalsService.updateGoalTime(timeObj.time, timeObj.goalId); //update goal loggedTime
  }

  handleUserClicked(uid: string) {
    //navigate to new user ID
    this.router.navigate([`/mygoals/${uid}`]);
  }

  setScroll() {
    //handle autoscroll
    this.newGoalScroll = false;
  }
}
