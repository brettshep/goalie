import { AuthService } from "../../auth/shared/auth.service";
import { Observable } from "rxjs/Observable";

import { Store } from "../../store";
import { AngularFireStorage } from "angularfire2/storage";
import { AngularFirestore } from "angularfire2/firestore";
import { Injectable } from "@angular/core";
import { switchMap, tap, take } from "rxjs/operators";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

export interface Goal {
  id: string;
  uid: string;
  created: number;
  text: string;
  loggedTime: number;
  image: string;
  complete: boolean;
}

@Injectable()
export class GoalsService {
  goals$: Observable<Goal[]>;
  currentId$: BehaviorSubject<string>;
  constructor(
    private fireStore: AngularFirestore,
    private storage: AngularFireStorage,
    private store: Store,
    private authService: AuthService
  ) {
    this.currentId$ = new BehaviorSubject(this.authService.UID);
    this.goals$ = this.currentId$.pipe(
      switchMap(uid => {
        return this.fireStore
          .collection<Goal>("myGoals", ref =>
            ref.where("uid", "==", uid).orderBy("created")
          )
          .valueChanges();
      })
    );
    // tap(goals => {
    //   this.store.set("goals" + this.currentUID$.value, goals);
    // })
  }

  get newId() {
    return this.fireStore.createId();
  }

  deleteGoal(goalId: string) {
    return this.fireStore
      .collection<Goal>("myGoals")
      .doc(goalId)
      .delete();
  }

  createNewGoal(goal: Goal) {
    return this.fireStore
      .collection<Goal[]>("myGoals")
      .doc(goal.id)
      .set(goal);
  }

  toggleGoalComplete(complete, goalId) {
    const currdoc = this.fireStore.collection("myGoals").doc(goalId);
    const completeRef = this.fireStore.collection("completeGoals");
    currdoc.ref.get().then(doc => {
      //set goal data
      let goal = doc.data();
      goal.created = Date.now();
      goal.complete = true;
      //push to other collection
      completeRef.doc(goal.id).set(goal);
      currdoc.delete();
    });
    // .set({ complete, created: Date.now() }, { merge: true });
  }

  updateGoalTime(time, goalId) {
    return this.fireStore
      .collection<Goal[]>("myGoals")
      .doc(goalId)
      .set({ loggedTime: time }, { merge: true });
  }

  setGoalImage(fileList, uid, goalId) {
    return this.uploadPicture(fileList, uid)
      .pipe(
        take(1),
        tap(url => {
          this.fireStore
            .doc(`myGoals/${goalId}`)
            .set({ image: url }, { merge: true });
        })
      )
      .subscribe();
  }

  uploadPicture(event: FileList, uid: string): Observable<string> {
    //no file
    if (event === null) return Observable.of(null);

    //file object
    const file = event.item(0);

    //client-side validation
    if (file.type.split("/")[0] != "image") {
      console.error("unsupported file type");
      return Observable.of(null);
      // return Promise.reject(new Error("unsupported file type"));
    }

    //file storage path
    const path = `goals/${uid}/${Date.now()}_${file.name}`;

    //optional metadata
    const customMetadata = { app: "Goalie" };

    let task = this.storage.upload(path, file, { customMetadata });

    return task.downloadURL();
  }
}
