import { Goal } from "../../goals.service";
import {
  Component,
  Input,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter
} from "@angular/core";

@Component({
  selector: "my-goals-list",
  styleUrls: ["my-goals-list.component.sass"],
  template: `
        <div class="wrapper" [class.noBar]="_currId !== uid">
            <div class = "my-goals-list" #goalBox>
                <div class="spacer">
                <div>
                    <div *ngFor="let goal of goals; trackBy: trackByFn;">
                            <goal 
                            [uid]="uid"
                            [_goal]="goal"
                            (delete)="deleteGoal($event)"
                            (picChoosen)="uploadPic($event)"
                            (updateDBTime)="updateDBTimeEmit($event,goal.id)"
                            (timerRunning)="timerRunningEmit($event)"
                            (toggleComplete)="toggleCompleteEmit($event,goal.id)"
                            ></goal>
                    </div>
                </div>
                </div>
            </div>
        </div>
    `
})
export class MyGoalsListComponent {
  //------VARS-------

  goals: Goal[] = [];
  newGoalScroll: boolean;
  changed: boolean = true;
  _currId: string = "";
  //---INPUT-OUTPUT-VIEWCHILD--
  @ViewChild("goalBox") goalBox: ElementRef;
  @Input() uid;
  @Input() //currId input
  set currId(value) {
    if (this._currId !== value) {
      this.changed = true;
    }
    this._currId = value;
  }
  @Input() //goal scroll input
  set _newGoalScroll(value) {
    this.newGoalScroll = value;
    if (this.newGoalScroll) {
      setTimeout(() => {
        this.scrollBottom();
        this.didScroll.emit();
      });
    }
  }
  @Input() //goals input
  set _goals(value: Goal[]) {
    if (!this.changed) {
      this.goals = value;
    } else {
      this.goals = [];
      for (let i = 0; i < value.length; i++) {
        let goal = value[i];
        setTimeout(() => {
          this.goals.push(goal);
        }, 100 * i);
      }
    }
    this.changed = false;
  }

  @Output() didScroll = new EventEmitter();
  @Output() delete = new EventEmitter<string>();
  @Output() picChoosen = new EventEmitter();
  @Output() updateDBTime = new EventEmitter();
  @Output() timerRunning = new EventEmitter<boolean>();
  @Output() toggleComplete = new EventEmitter();

  toggleCompleteEmit(complete: boolean, id: string) {
    this.toggleComplete.emit({ complete, id });
  }

  timerRunningEmit(event) {
    this.timerRunning.emit(event);
  }

  updateDBTimeEmit(time, goalId) {
    this.updateDBTime.emit({ time, goalId });
  }

  deleteGoal(id: string) {
    this.delete.emit(id);
  }

  uploadPic(event) {
    this.picChoosen.emit(event);
  }

  scrollBottom() {
    this.goalBox.nativeElement.scrollTop = this.goalBox.nativeElement.scrollHeight;
  }

  trackByFn(index, item) {
    return item.id; // or item.id
  }
}
