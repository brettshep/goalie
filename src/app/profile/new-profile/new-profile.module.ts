import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { ProfileSharedModule } from "./../shared/profile-shared.module";

//containers
import { NewProfileComponent } from "./new-profile.component";

export const ROUTES: Routes = [{ path: "", component: NewProfileComponent }];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ROUTES), ProfileSharedModule],
  declarations: [NewProfileComponent],
  providers: []
})
export class NewProfileModule {}
