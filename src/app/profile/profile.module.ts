import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";

import { ProfileSharedModule } from "./shared/profile-shared.module";

export const ROUTES: Routes = [
  {
    path: "edit-profile",
    //canActivate: [AuthGuard],
    loadChildren: "./edit-profile/edit-profile.module#EditProfileModule"
  },
  {
    path: "new-profile",
    //canActivate: [AuthGuard],
    loadChildren: "./new-profile/new-profile.module#NewProfileModule"
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    ProfileSharedModule.forRoot()
  ],
  declarations: []
})
export class ProfileModule {}
