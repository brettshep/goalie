import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";

import { ProfileSharedModule } from "./../shared/profile-shared.module";

//containers
import { EditProfileComponent } from "./edit-profile.component";

export const ROUTES: Routes = [{ path: "", component: EditProfileComponent }];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ROUTES), ProfileSharedModule],
  declarations: [EditProfileComponent],
  providers: []
})
export class EditProfileModule {}
