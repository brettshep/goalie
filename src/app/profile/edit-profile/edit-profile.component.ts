import { Store } from "./../../store";
import { Observable } from "rxjs/Observable";
import { AuthService, User } from "./../../auth/shared/auth.service";
import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { ProfileService } from "./../shared/profile.service";

@Component({
  selector: "edit-profile",
  template: `
      <div class="edit-profile" *ngIf="(user$ | async) as user; else loading">
       <profile-form
       [_user] = "user"
       (picUpdate)="updateProfilePic($event)"
       (nameUpdate)="updateUserName($event)"
       (deleteAccount)="deleteAccount()"
       >
          <h1>Edit Profile</h1>
          <div class="error" *ngIf ="error">
            {{error}}
          </div>
          <button class="delete" type = "button">Delete Account</button>
          <button class="submit" type = "submit">Continue</button>
       </profile-form>
      </div>
      <ng-template #loading>
        Loading Profile...
        </ng-template>
    `
})
export class EditProfileComponent {
  error: string;
  user$: Observable<User>;

  constructor(
    private router: Router,
    private authService: AuthService,
    private profileService: ProfileService,
    private store: Store
  ) {}

  ngOnInit() {
    this.user$ = this.store.select<User>("user");
  }

  async deleteAccount() {
    try {
      await this.authService.deleteAccount();
      this.router.navigate(["/auth"]);
    } catch (err) {
      this.error = err.message;
    }
  }
  async updateProfilePic(fileList: FileList) {
    try {
      await this.profileService.uploadProfilePic(
        fileList,
        this.authService.UID
      );
    } catch (err) {
      this.error = err.message;
    }
  }

  async updateUserName(name: string) {
    try {
      await this.profileService.setUserName(name, this.authService.UID);
      this.router.navigate(["/mygoals"]);
    } catch (err) {
      this.error = err.message;
    }
  }
}
