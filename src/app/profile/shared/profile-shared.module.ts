import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

//componenets
import { ProfileFormComponent } from "./profile-form.component";
//directive
import { DropZoneModule } from "./../../shared/dropZone.module";

//service
import { ProfileService } from "./profile.service";

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, DropZoneModule],
  declarations: [ProfileFormComponent],
  exports: [ProfileFormComponent]
})
export class ProfileSharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ProfileSharedModule,
      providers: [ProfileService]
    };
  }
}
