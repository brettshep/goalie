import { Injectable } from "@angular/core";
import { AngularFireStorage } from "angularfire2/storage";
import { AngularFirestore } from "angularfire2/firestore";
import { take, tap } from "rxjs/operators";

@Injectable()
export class ProfileService {
  constructor(
    private storage: AngularFireStorage,
    private firestore: AngularFirestore
  ) {}

  uploadProfilePic(event: FileList, uid: string) {
    //file object
    const file = event.item(0);
    //client-side validation
    if (file.type.split("/")[0] != "image") {
      //do error
      return Promise.reject(new Error("unsupported file type"));
    }
    //file storage path
    const path = `${uid}/${new Date().getTime()}_${file.name}`;
    //optional metadata
    const customMetadata = { app: "Goalie" };

    let task = this.storage.upload(path, file, { customMetadata });

    task
      .downloadURL()
      .pipe(
        take(1),
        tap(url => {
          this.firestore
            .doc(`users/${uid}`)
            .set({ profilePicURL: url }, { merge: true });
        })
      )
      .subscribe();
  }

  setUserName(name: string, uid: string) {
    return this.firestore
      .doc(`users/${uid}`)
      .set({ username: name }, { merge: true });
  }
}
