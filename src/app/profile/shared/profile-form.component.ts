import { User } from "./../../auth/shared/auth.service";
import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy
} from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "profile-form",
  styleUrls: ["profile-form.component.sass"],
  template: `
      <div class="profile-form">
        <form [formGroup] = 'form' (ngSubmit) = 'nameSubmit()'>
        
          <ng-content select = "h1"></ng-content>
              
          <div 
          class="pic-upload" 
          dropZone
          (hovered)="toggleHover($event)"
          (dropped)="uploadPic($event)"
          [class.hovering] = "isHovering"
          >
                <input type="file" (change)="uploadPic($event.target.files)">
                <div class="image" [ngStyle]="{'background-image': 'url(' + profileURL + ')'}"></div>
          </div>
            
          <div class="buttonText">
            Click or drag to add a profile picture
          </div>
            
          <label class="username-label" [class.isFocused]="userNameFocused">
          Username
            <input
            (focus)="userNameFocused = true"    
            (blur)="userNameFocused = false"  
            type="text"         
            formControlName="username"
            >
          </label>

          <div class="error" *ngIf = "usernameInvalid">
             Username is Required.
          </div>

          <ng-content select=".error"></ng-content>
          <div class="button-cont">
            <ng-content select=".submit"></ng-content>
          </div>
          <div class="button-cont">
            <div (click)="toggleDelete = !toggleDelete">
              <ng-content select=".delete"></ng-content>
            </div>
            <div *ngIf="!toggleDelete else areyousure"></div>
          </div>

          <ng-template #areyousure>
            <button type = "button" class="no-btn-style confirm"(click)="deleteUserAccount()">Confirm</button>
            <button type = "button" class="no-btn-style cancel"(click)="toggleDelete = !toggleDelete">Cancel</button>    
          </ng-template>        

        </form>
      </div>
    `
})
export class ProfileFormComponent {
  user: User;
  profileURL: string = "/img/loadingDarkSmall.svg";
  toggleDelete: boolean = false;

  @Input()
  set _user(value: User) {
    this.user = value;
    {
      this.profileURL = value.profilePicURL;
      this.form.setValue({ username: value.username });
    }
  }
  //state for input css toggle
  userNameFocused: boolean = false;
  //state for dropzone css toggle
  isHovering: boolean;

  @Output() nameUpdate = new EventEmitter<string>();
  @Output() picUpdate = new EventEmitter<FileList>();
  @Output() deleteAccount = new EventEmitter();

  form = this.fb.group({
    username: ["", Validators.required]
  });

  constructor(private fb: FormBuilder) {}

  deleteUserAccount() {
    this.deleteAccount.emit();
  }
  uploadPic(fileList) {
    this.picUpdate.emit(fileList);
    //set loading symbol
    this.profileURL = "/img/loadingDarkSmall.svg";
  }

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  nameSubmit() {
    if (this.form.valid) {
      //emit
      this.nameUpdate.emit(this.form.value.username);
    }
  }

  get usernameInvalid() {
    const control = this.form.get("username");
    return control.hasError("required") && control.touched;
  }
}
