import {
  Directive,
  ElementRef,
  Output,
  EventEmitter,
  HostListener
} from "@angular/core";

@Directive({
  selector: "[clickedOutside]"
})
export class ClickedOutsideDirective {
  constructor(private _elementRef: ElementRef) {}

  @Output() public clickedOutside = new EventEmitter<boolean>();

  @HostListener("document:click", ["$event", "$event.target"])
  public onClick(event: MouseEvent, targetElement: HTMLElement): void {
    if (!targetElement && event) {
      return;
    }

    const clickedInside = this._elementRef.nativeElement.contains(
      targetElement
    );
    if (!clickedInside) {
      this.clickedOutside.emit(true);
    }
  }
}
