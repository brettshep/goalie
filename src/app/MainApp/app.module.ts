import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

// import { CustomReuseStrategy } from "./customReuseStrategy";
// import { RouteReuseStrategy } from "@angular/router";
import { Routes, RouterModule } from "@angular/router";

import { Store } from "./../store";

// feature modules
import { AuthModule } from "../auth/auth.module";
import { GoalsModule } from "./../goals/goals.module";
import { ProfileModule } from "./../profile/profile.module";
import { BackgroundModule } from "./containers/background/background.module";

// containers
import { AppComponent } from "./containers/app.component";

// components
import { AppHeaderComponent } from "./components/app-header/app-header.component";

//angular fire
import { AngularFireModule, FirebaseAppConfig } from "angularfire2";
import { AngularFirestoreModule } from "angularfire2/firestore";
import { AngularFireStorageModule } from "angularfire2/storage";

export const config: FirebaseAppConfig = {
  apiKey: "AIzaSyCfXZrxl51Co9ImWKJ037N7q_vQPU2HIbs",
  authDomain: "goalie-4a2e8.firebaseapp.com",
  databaseURL: "https://goalie-4a2e8.firebaseio.com",
  projectId: "goalie-4a2e8",
  storageBucket: "goalie-4a2e8.appspot.com",
  messagingSenderId: "899571410988"
};

// routes
export const ROUTES: Routes = [
  { path: "", pathMatch: "full", redirectTo: "mygoals" },
  {
    path: "messages",
    loadChildren: "./../messages/messages.module#MessagesModule"
  },
  { path: "**", redirectTo: "mygoals" }
];

@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES),
    AuthModule,
    GoalsModule,
    ProfileModule,
    AngularFireModule.initializeApp(config),
    AngularFirestoreModule,
    AngularFireStorageModule,
    BackgroundModule
  ],
  declarations: [AppComponent, AppHeaderComponent],
  providers: [Store],
  bootstrap: [AppComponent]
})
export class AppModule {}
