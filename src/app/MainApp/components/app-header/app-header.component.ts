import {
  Component,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  Input
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "app-header",
  styleUrls: ["app-header.component.sass"],
  template: `
        <div class = "app-header" [ngStyle]="{display: flexOrNot}">
            <div class="logoCont flexCont">
            <a routerLink = "/mygoals" routerLinkActive = "active">
                <img src="img/logo.png">  
            </a>     
            </div>
            <div class="navCont flexCont" *ngIf = "authenticated">   
                  <a routerLink = "/mygoals" routerLinkActive = "active" class = "svgCont">
                    <img src="/img/goalsNav.svg">
                    <img src="/img/goalsNavActive.svg" class="activeColor">
                  </a>
                  <a routerLink = "/complete" routerLinkActive = "active" class = "svgCont">
                    <img src="/img/calendarNav.svg">
                    <img src="/img/calendarNavActive.svg" class="activeColor">
                  </a>
                  <a routerLink = "/messages" routerLinkActive = "active" class = "svgCont">
                    <img src="/img/messagesNav.svg">
                    <img src="/img/messagesNavActive.svg" class="activeColor">
                  </a>
            </div>
            <div class="settingsCont flexCont" *ngIf = "authenticated">   
                  <div class = "svgCont" (click)="backgroundToggle = !backgroundToggle">
                    <img src="img/theme.svg">
                    <img src="img/themeActive.svg" class="activeColor">       
                      <background *ngIf="backgroundToggle" >
                      </background>          
                  </div>     
                  <a routerLink = "/edit-profile" routerLinkActive = "active" class = "svgCont">
                    <img src="img/settings.svg">
                    <img src="img/settingsActive.svg" class="activeColor">                 
                  </a>     
                  <div class = "svgCont">
                    <img src="img/logout.svg"  (click) = "logoutUser()">
                    <img src="img/logoutActive.svg"  (click) = "logoutUser()" class="activeColor">                 
                  </div>     
            </div>
        </div>
    `
})
export class AppHeaderComponent {
  @Input() authenticated: boolean;
  @Output() logout = new EventEmitter<any>();
  backgroundToggle = false;
  logoutUser() {
    this.logout.emit();
  }

  get flexOrNot() {
    if (this.authenticated) {
      return "flex";
    } else {
      return "block";
    }
  }
}
