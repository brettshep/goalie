import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
//component
import { ScrollModalComponent } from "./scrollModal/scroll-modal.component";
import { BackGroundComponent } from "./background.component";

//service
import { UnsplashService } from "./unsplash.service";

//directive
import { ScrollableModule } from "../../../shared/scrollable.module";

@NgModule({
  imports: [CommonModule, ScrollableModule],
  declarations: [BackGroundComponent, ScrollModalComponent],
  exports: [BackGroundComponent, ScrollModalComponent],
  providers: [UnsplashService]
})
export class BackgroundModule {}
