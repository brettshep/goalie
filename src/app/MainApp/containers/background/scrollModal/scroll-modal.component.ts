import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy
} from "@angular/core";

@Component({
  selector: "scroll-modal",
  styleUrls: ["scroll-modal.component.sass"],
  template: `
        <div class="scroll-modal"
        scrollable
        (scrollPosition)="scrollHandler($event)"
        >
        <div class="imageContDef" (click)="emitSrc('')">
            Default
        </div>
            <div *ngFor="let photo of _photos">
                <div class="imageCont">
                    <img class="loading" src="/img/loadingDarkSmall.svg" >
                    <img class="image" [src]="photo" (click)="emitSrc(photo)">
                </div>
            </div>
        </div>
    `
})
export class ScrollModalComponent {
  _photos;
  @Input()
  set photos(value) {
    this._photos = value;
  }

  @Output() moreMessages = new EventEmitter();
  @Output() setBG = new EventEmitter<string>();

  emitSrc(src: string) {
    this.setBG.emit(src);
  }

  scrollHandler(event) {
    if (event === "bottom") {
      this.moreMessages.emit();
    }
  }
}
