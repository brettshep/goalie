import { Injectable } from "@angular/core";
import { take, tap, merge } from "rxjs/Operators";
import { Subject } from "rxjs/Subject";

@Injectable()
export class UnsplashService {
  photos$: Subject<string[]>;
  photos: string[] = [];
  constructor() {
    this.photos$ = new Subject();
  }

  fetchPhotos() {
    var _this = this;
    for (let i = 0; i < 5; i++) {
      fetch(`https://source.unsplash.com/1920x1080?sig=${i}`).then(function(
        response
      ) {
        if (_this.photos.includes(response.url)) {
          i--;
        } else {
          _this.photos.push(response.url);
          _this.photos$.next(_this.photos);
        }
      });
    }
  }

  addFivePhotos() {
    this.fetchPhotos();
  }

  clearPhotos() {
    this.photos = [];
  }
}
