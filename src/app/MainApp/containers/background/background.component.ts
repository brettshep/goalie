import { Component } from "@angular/core";
import { UnsplashService } from "./unsplash.service";
import { AuthService } from "../../../auth/shared/auth.service";

@Component({
  selector: "background",
  styleUrls: ["background.component.sass"],
  template: `
        <div>
        
            <scroll-modal
            [photos]="(photos$ | async)"
            (moreMessages)="fetchMorePhotos()"
            (setBG)="setBackground($event)"
            >
            </scroll-modal>
        </div>
    `
})
export class BackGroundComponent {
  constructor(
    private unsplash: UnsplashService,
    private authService: AuthService
  ) {}
  photos$;
  ngOnInit() {
    this.unsplash.clearPhotos();
    this.unsplash.fetchPhotos();
    this.photos$ = this.unsplash.photos$;
  }

  setBackground(src) {
    this.authService.setUserBackGround(src);
  }

  fetchMorePhotos() {
    this.unsplash.fetchPhotos();
  }
}
