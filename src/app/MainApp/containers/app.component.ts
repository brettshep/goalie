import { Router } from "@angular/router";
import { Component } from "@angular/core";
import { AuthService, User } from "./../../auth/shared/auth.service";
import { Store } from "./../../store";
import { Observable } from "rxjs/Observable";
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: "app-root",
  styleUrls: ["app.component.sass"],
  template: `
  <div>
    <div class="bgAlways"></div>
    <div *ngIf="(user$ | async) as user; else notLoggedIn">
        <div class="bg" [ngStyle]="{'background-image': 'url(' + bgImage(user) + ')'}"></div>
        <app-header [authenticated] = "true" (logout) = "onLogout()" ></app-header>
      </div>
      
      <div class="wrapper">
        <router-outlet></router-outlet>
      </div>
       
      <ng-template #notLoggedIn>
          <app-header [authenticated] = "false" (logout) = "onLogout()" ></app-header>
      </ng-template>

    </div>
  `
})
export class AppComponent {
  user$: Observable<User>;
  usersList$: Observable<User[]>;
  subscriptions: Subscription[] = [];

  constructor(
    private store: Store,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    //user
    this.subscriptions.push(this.authService.user$.subscribe());
    this.user$ = this.store.select<User>("user");
    //users list
    this.subscriptions.push(this.authService.usersList$.subscribe());
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  bgImage(user: User) {
    if (user.bg) {
      return user.bg;
    } else {
      return "";
    }
  }

  async onLogout() {
    await this.authService.logOutUser();
    //redirect
    this.router.navigate(["/auth/login"]);
  }
}
