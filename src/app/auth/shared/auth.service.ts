import { Store } from "./../../store";
import { Injectable } from "@angular/core";
import { AngularFireAuth } from "angularfire2/auth";
import {
  AngularFirestore,
  AngularFirestoreDocument
} from "angularfire2/firestore";
import * as Firebase from "firebase/app";
import { Observable } from "rxjs/Observable";
import { tap, switchMap } from "rxjs/operators";

export interface User {
  uid: string;
  email: string;
  authenticated: boolean;
  profilePicURL: string;
  username: string;
  bg: string;
}

@Injectable()
export class AuthService {
  constructor(
    private afAuth: AngularFireAuth,
    private fireStore: AngularFirestore,
    private store: Store
  ) {}

  //set user in store
  private firstTimeUser: boolean;

  user$ = this.afAuth.authState.pipe(
    switchMap(user => {
      if (user) {
        return this.fireStore.doc<User>(`users/${user.uid}`).valueChanges();
      } else {
        return Observable.of(null);
      }
    }),
    tap(user => {
      this.store.set("user", user);
    })
  );

  setUserBackGround(bgSrc: string) {
    this.fireStore.doc(`users/${this.UID}`).set({ bg: bgSrc }, { merge: true });
  }

  usersList$ = this.fireStore
    .collection("users")
    .valueChanges()
    .pipe(
      tap((users: User[]) => {
        this.store.set("usersList", users);
      })
    );

  get authState() {
    return this.afAuth.authState;
  }

  get user() {
    return this.afAuth.auth.currentUser;
  }

  get FirstTimeUser() {
    return this.firstTimeUser;
  }

  get UID() {
    return this.afAuth.auth.currentUser.uid;
  }

  googleLogin() {
    const provider = new Firebase.auth.GoogleAuthProvider();
    return this.oAuthLogin(provider);
  }

  private oAuthLogin(provider: any) {
    return this.afAuth.auth.signInWithPopup(provider).then(credential => {
      //set default data if new user
      if (credential.additionalUserInfo.isNewUser) {
        this.firstTimeUser = true;
        this.setUserData(credential.user, true);
      } else {
        this.firstTimeUser = false;
      }
    });
  }

  //update user info in Firestore
  private setUserData(user: any, google: boolean) {
    const userRef: AngularFirestoreDocument<any> = this.fireStore.doc(
      `users/${user.uid}`
    );
    let data: User;
    if (!google) {
      data = {
        uid: user.uid,
        email: user.email,
        authenticated: true,
        profilePicURL: "/img/defaultProfilePic.png",
        username: "",
        bg: ""
      };
    } else {
      data = {
        uid: user.uid,
        email: user.email,
        authenticated: true,
        profilePicURL: user.photoURL,
        username: user.displayName,
        bg: ""
      };
    }

    return userRef.set(data, { merge: true });
  }

  createUser(email: string, password: string) {
    this.firstTimeUser = true;
    return this.afAuth.auth
      .createUserWithEmailAndPassword(email, password)
      .then(credential => {
        //set default data if new user
        this.setUserData(credential, false);
      });
  }

  loginUser(email: string, password: string) {
    this.firstTimeUser = false;
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  logOutUser() {
    return this.afAuth.auth.signOut();
  }

  deleteAccount() {
    const uid = this.UID;

    const mygoals = this.fireStore.collection("myGoals");
    const completeGoals = this.fireStore.collection("completeGoals");

    const messages = this.fireStore.collection("messages");

    const user = this.fireStore.collection("users");

    return this.afAuth.auth.currentUser.delete().then(() => {
      //delete user firestorage
      user.ref
        .where("uid", "==", uid)
        .get()
        .then(snapshot => {
          snapshot.forEach(doc => {
            doc.ref.delete();
          });
        });
      //delete myGoals
      mygoals.ref
        .where("uid", "==", uid)
        .get()
        .then(snapshot => {
          snapshot.forEach(doc => {
            doc.ref.delete();
          });
        });
      //delete completeGoals
      completeGoals.ref
        .where("uid", "==", uid)
        .get()
        .then(snapshot => {
          snapshot.forEach(doc => {
            doc.ref.delete();
          });
        });
      //delete messages
      messages.ref
        .where("uid", "==", uid)
        .get()
        .then(snapshot => {
          snapshot.forEach(doc => {
            doc.ref.delete();
          });
        });
    });
  }
}
