import { Router } from "@angular/router";
import { FormGroup } from "@angular/forms";
import { Component } from "@angular/core";
import { AuthService } from "./../shared/auth.service";
@Component({
  selector: "login",
  template: `
        <div>
            <auth-form (submitted) = "loginUser($event)" (google) = "googleContinue()">
                <h1>Welcome Back!</h1>
                <div class="error" *ngIf ="error">
                 {{error}}
                </div>
                <button class = "actionBtn" type = "submit">Login</button>
                <a routerLink = "/auth/register">Not Registered?</a>
            </auth-form>
        </div>
    `
})
export class LoginComponent {
  constructor(private authService: AuthService, private router: Router) {}

  error: string;

  async loginUser(event: FormGroup) {
    const { email, password } = event.value;
    try {
      await this.authService.loginUser(email, password);
      this.router.navigate(["/mygoals"]);
    } catch (err) {
      this.error = err.message;
    }
  }

  async googleContinue() {
    try {
      await this.authService.googleLogin();
      if (this.authService.FirstTimeUser) {
        this.router.navigate(["/new-profile"]);
      } else {
        this.router.navigate(["/mygoals"]);
      }
    } catch (err) {
      this.error = err.message;
    }
  }
}
