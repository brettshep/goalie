import { Router } from "@angular/router";
import { FormGroup } from "@angular/forms";
import { Component } from "@angular/core";
import { AuthService } from "./../shared/auth.service";
@Component({
  selector: "register",
  template: `
        <div>       
        <auth-form (submitted) = "registerUser($event)" (google) = "googleContinue()">
          <h1>Register</h1>
          <div class="error" *ngIf ="error">
            {{error}}
          </div>
          <button class = "actionBtn" type = "submit">Create Account</button>
          <a routerLink = "/auth/login">Have an account?</a>
        </auth-form>
        </div>
    `
})
export class RegisterComponent {
  constructor(private authService: AuthService, private router: Router) {}

  error: string;

  async registerUser(event: FormGroup) {
    const { email, password } = event.value;
    try {
      await this.authService.createUser(email, password);
      this.router.navigate(["/new-profile"]);
    } catch (err) {
      this.error = err.message;
    }
  }

  async googleContinue() {
    try {
      await this.authService.googleLogin();
      if (this.authService.FirstTimeUser) {
        this.router.navigate(["/new-profile"]);
      } else {
        this.router.navigate(["/mygoals"]);
      }
    } catch (err) {
      this.error = err.message;
    }
  }
}
