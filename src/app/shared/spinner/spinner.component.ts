import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "spinner",
  styleUrls: ["spinner.component.sass"],
  template: `
    <div class="spinner" [ngStyle]="{ height: size, width: size}">
        <div class="dot1" [ngStyle]="{'background-color': color}"></div>
        <div class="dot2" [ngStyle]="{'background-color': color}"></div>
  </div>
    `
})
export class SpinnerComponent {
  @Input() color: string;
  @Input() size: string;
}
