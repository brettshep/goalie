import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

//component
import { InputBarComponent } from "./input-bar/input-bar.component";
import { EmojiComponent } from "./input-bar/emoji/emoji.component";

//directive
import { DropZoneModule } from "./dropZone.module";
import { ClickedOutsideModule } from "./clickedOutside.module";

@NgModule({
  imports: [CommonModule, DropZoneModule, ClickedOutsideModule],
  declarations: [InputBarComponent, EmojiComponent],
  exports: [InputBarComponent]
})
export class InputBarModule {}
