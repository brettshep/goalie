import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  ChangeDetectionStrategy
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "input-bar",
  styleUrls: ["input-bar.component.sass", "input-bar.component.css"],
  template: `
        <div 
        class = "input-bar" 
        dropZone
        (hovered)="toggleHover($event)"
        (dropped)="uploadPic($event)"
        [class.hovering] = "isHovering"
        >
            <div>
              <img src="/img/loadingDarkSmall.svg" class="loadingImage" [class.hidden]="isLoading ? false : true">
            </div>
            <div #textArea contenteditable="true" class="text-area" (keyup.enter)="textSubmit()"></div>
            <div class="button-containers">
                    <img src="/img/emoji.svg" (click) = "emojiToggled = !emojiToggled">
                    <div class="image-input" *ngIf="showImageAdd">
                        <input type="file" (change)="uploadPic($event.target.files)">
                        <img src="/img/picture.svg">
                    </div>
                    <img src="/img/plus.svg" (click)="textSubmit()">
            </div>

            <emoji
             *ngIf = "emojiToggled"
            (symbol)="inputEmoji($event)"
            (close) = "emojiToggled = false"
            >
            </emoji>

        </div>
    `
})
export class InputBarComponent {
  //state for dropzone css toggle
  emojiToggled: boolean = false;
  isHovering: boolean;
  @Input() showImageAdd;
  @Input() placeHolder;
  @Input() isLoading;
  @ViewChild("textArea") textArea: ElementRef;

  @Output() picSend = new EventEmitter<FileList>();
  @Output() textSend = new EventEmitter<string>();

  toggleHover(event: boolean) {
    this.isHovering = event;
  }
  uploadPic(filelist) {
    this.picSend.emit(filelist);
  }

  textSubmit() {
    this.textSend.emit(this.textArea.nativeElement.textContent);
    this.textArea.nativeElement.textContent = "";
  }
  ngOnInit() {
    document.execCommand("defaultParagraphSeparator", false, "p");
    this.textArea.nativeElement.style.cssText = `--place-holder: '${
      this.placeHolder
    }' `;
  }

  inputEmoji(emoji) {
    this.textArea.nativeElement.textContent =
      this.textArea.nativeElement.textContent + emoji;
  }
}
