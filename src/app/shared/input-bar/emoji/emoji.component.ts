import {
  Component,
  ChangeDetectionStrategy,
  Output,
  EventEmitter
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "emoji",
  styleUrls: ["emoji.component.sass"],
  template: `
        <div
        class = "emoji"
        clickedOutside
        (clickedOutside) = "toggle()">
            <p *ngFor = "let emoji of emojiArray" (click) = "sendEmoji(emoji)">
                {{emoji}}
            </p>
        </div>
    `
})
export class EmojiComponent {
  @Output() symbol = new EventEmitter<string>();
  @Output() close = new EventEmitter();
  clickedYet: boolean = true;
  emojiArray: string[] = [
    "😀",
    "😁",
    "😂",
    "🤣",
    "😃",
    "😄",
    "😅",
    "😆",
    "😉",
    "😊",
    "😋",
    "😎",
    "😍",
    "😘",
    "😗",
    "😙",
    "😚",
    "🙂",
    "🤗",
    "🤩",
    "🤔",
    "🤨",
    "😐",
    "😑",
    "😶",
    "🙄",
    "😏",
    "😣",
    "😥",
    "😮",
    "🤐",
    "😯",
    "😪",
    "😫",
    "😴",
    "😌",
    "😛",
    "😜",
    "😝",
    "🤤",
    "😒",
    "😓",
    "😔",
    "😕",
    "🙃",
    "🤑",
    "😲",
    "☹️",
    "🙁",
    "😖",
    "😞",
    "😟",
    "😤",
    "😢",
    "😭",
    "😦",
    "😧",
    "😨",
    "😩",
    "🤯",
    "😬",
    "😰",
    "😱",
    "😳",
    "🤪",
    "😵",
    "😡",
    "😠",
    "🤬",
    "😷",
    "🤒",
    "🤕",
    "🤢",
    "🤮",
    "🤧",
    "😇",
    "🤠",
    "🤡",
    "🤥",
    "🤫",
    "🤭",
    "🧐",
    "🤓",
    "💀",
    "👻",
    "👽",
    "🤖",
    "💩",
    "🐢",
    "😺",
    "😸",
    "😹",
    "😻",
    "😼",
    "😽",
    "🙀"
  ];
  sendEmoji(item: string) {
    this.symbol.emit(item);
  }

  toggle() {
    this.clickedYet = !this.clickedYet;
    if (this.clickedYet) this.close.emit();
  }
}
