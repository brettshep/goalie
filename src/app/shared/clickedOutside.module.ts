import { NgModule } from "@angular/core";
import { ClickedOutsideDirective } from "./../directives/clicked-outside.directive";

@NgModule({
  imports: [],
  declarations: [ClickedOutsideDirective],
  exports: [ClickedOutsideDirective]
})
export class ClickedOutsideModule {}
