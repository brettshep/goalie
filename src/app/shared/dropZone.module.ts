import { NgModule } from "@angular/core";
import { DropZoneDirective } from "./../directives/dropzone.directive";

@NgModule({
  imports: [],
  declarations: [DropZoneDirective],
  exports: [DropZoneDirective]
})
export class DropZoneModule {}
