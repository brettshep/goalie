import { NgModule } from "@angular/core";
import { ScrollableDirective } from "../directives/scrollable.directive";

@NgModule({
  imports: [],
  declarations: [ScrollableDirective],
  exports: [ScrollableDirective]
})
export class ScrollableModule {}
